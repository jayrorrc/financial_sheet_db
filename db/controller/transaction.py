'''
Control of Transaction Mongo Document
'''

import datetime


def convert_to_float(number):
    '''Convert integer to float'''

    if isinstance(number, int):
        return number + 0.0

    return number


def save_transaction(connection, database, data):
    '''
    Save one Transaction from JSON
    '''

    [day, month, year] = map(int, data['date'].split('/'))
    date = datetime.datetime(
        year,
        month,
        day
    )

    value = convert_to_float(data['value'])

    query = {
        'transaction_type': data['transaction_type'],
        'date': date,
        'name': data['name'],
        'value': value
    }

    collection = connection[database].transactions
    transaction = collection.Transaction.one(query)

    if transaction is None:
        transaction = collection.Transaction()

        transaction.transaction_type = data['transaction_type']

        [day, month, year] = map(int, data['date'].split('/'))
        transaction.date = date

        transaction.name = data['name']
        transaction.value = value

        if 'ir' in data:
            transaction.ir = convert_to_float(data['ir'])

        if 'comp_ir' in data:
            transaction.comp_ir = convert_to_float(data['comp_ir'])

        if 'iof' in data:
            transaction.iof = convert_to_float(data['iof'])

        if 'num_quotes' in data:
            transaction.num_quotes = convert_to_float(data['num_quotes'])

    if 'children' in data:
        old_children = transaction.children
        data_children = data['children']

        child = lambda x: save_transaction(connection, database, x['data'])
        test_if_is_not_in_children = lambda x: x not in old_children

        new_children = filter(
            test_if_is_not_in_children,
            map(child, data_children)
        )

        transaction.children += new_children

    transaction.save()

    return transaction
