'''
Control of Service Mongo Document
'''
from db.controller import transaction


def save_service(connection, database, data):
    '''
    Save one Service from JSON
    '''

    query = {
        'name': data['name'],
        'service_type': data['service_type'],
    }

    collection = connection[database].services
    service = collection.Service.one(query)

    if service is None:
        service = collection.Service()

        service.name = data['name']
        service.service_type = data['service_type']

        if 'cnpj' in data:
            service.cnpj = data['cnpj']

    if 'history' in data:
        old_history = service.history
        data_history = data['history']

        history = lambda x: transaction.save_transaction(
            connection,
            database,
            x['data']
        )

        test_if_is_not_in_history = lambda x: x not in old_history

        new_history = filter(
            test_if_is_not_in_history,
            map(history, data_history)
        )

        service.history += new_history

    service.save()

    return service
