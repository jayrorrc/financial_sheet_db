'''
Model of Service Mongo Document
'''

import datetime
from numpy import isclose
from mongokit import Document


def add(x, y):
    '''Sum two number even None'''

    if x is None:
        return y
    elif y is None:
        return x
    else:
        return x + y


def add_transactions_values(acc, T):
    '''Add values of two Transactions'''

    ir = add(acc[0], T['ir'])
    iof = add(acc[1], T['iof'])
    value = add(acc[2], T['value'])
    comp_ir = add(acc[3], T['comp_ir'])
    num_quotes = add(acc[4], T['num_quotes'])

    return [ir, iof, value, comp_ir, num_quotes]


def is_transaction(test, transaction):
    '''Test if Document is a valid Transaction'''

    is_present = 'transaction_type' in transaction
    is_none = transaction['transaction_type'] is not None

    return test and is_present and is_none


def children_validator(list_of_transactions):
    '''Validate child transactions'''

    return reduce(is_transaction, list_of_transactions, True)


class Transaction(Document):
    '''
    Deficinition of Service Document
    '''

    use_schemaless = True
    use_dot_notation = True

    structure = {
        'transaction_type': unicode,
        'date': datetime.datetime,
        'name': unicode,
        'value': float,
        'ir': float,
        'comp_ir': float,
        'iof': float,
        'num_quotes': float,
        'children': [
            Document
        ],
    }
    required_fields = [
        'transaction_type',
        'date',
        'name',
        'value',
    ]
    validators = {
        'children': children_validator,
    }
    default_values = {
        'ir': None,
        'comp_ir': None,
        'iof': None,
        'num_quotes': None,
        'children': [],
    }

    def __repr__(self):
        return '<Transaction %r>' % (self.name)

    def validate(self, *args, **kwargs):
        '''
        Validate that the sum of children's values are equals parent's values
        '''

        children = self['children']
        if children:
            init_values = [None, None, None, None, None]
            sum_children = reduce(
                add_transactions_values, children, init_values
            )

            ir = sum_children[0]
            iof = sum_children[1]
            value = sum_children[2]
            comp_ir = sum_children[3]
            num_quotes = sum_children[4]

            parent_ir = self['ir']
            parent_iof = self['iof']
            parent_value = self['value']
            parent_comp_ir = self['comp_ir']
            parent_num_quotes = self['num_quotes']

            equals = lambda x, y: (x is None and y is None) or isclose(x, y)

            assert equals(parent_ir, ir)
            assert equals(parent_iof, iof)
            assert equals(parent_value, value)
            assert equals(parent_comp_ir, comp_ir)
            assert equals(parent_num_quotes, num_quotes)

        super(Transaction, self).validate(*args, **kwargs)
