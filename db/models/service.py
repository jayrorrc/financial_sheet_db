#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
Model of Service Mongo Document
'''

import re
from mongokit import Document
from db.models import transaction


def validar_cnpj(cnpj):
    '''Validate CNPJ'''

    cnpj = ''.join(re.findall(r'(\d)', cnpj))

    if (not cnpj) or (len(cnpj) < 14):
        return False

    '''
    Take the 12 first digits and generate the remaining 2
    '''
    inteiros = map(int, cnpj)
    novo = inteiros[:12]

    prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
    while len(novo) < 14:
        r = sum([x*y for (x, y) in zip(novo, prod)]) % 11
        if r > 1:
            f = 11 - r
        else:
            f = 0
        novo.append(f)
        prod.insert(0, 6)

    if novo == inteiros:
        return True
    return False


class Service(Document):
    '''
    Deficinition of Service Document
    '''

    use_autorefs = True
    use_schemaless = True
    use_dot_notation = True

    structure = {
        'name': unicode,
        'cnpj': unicode,
        'service_type': unicode,
        'history': [
            transaction.Transaction
        ]
    }
    required_fields = [
        'name',
        'service_type',
    ]
    validators = {
        'cnpj': validar_cnpj,
    }

    def __repr__(self):
        return '<Service %r>' % (self.name)
