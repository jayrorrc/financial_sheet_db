'''
Routes to access the database
'''

import json
from flask import Flask, request
from mongokit import Connection

# Models
from db.models import service
from db.models import transaction

# Controllers
from db.controller import service as service_controller
from db.controller import transaction as transaction_controller

# configuration
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DATABASE = 'test'

APP = Flask(__name__)
APP.config.from_object(__name__)

# connect to the database
CONNECTION = Connection(APP.config['MONGODB_HOST'],
                        APP.config['MONGODB_PORT'])

CONNECTION.register([
    service.Service,
    transaction.Transaction,
])


@APP.route('/models/save', methods=['POST'])
def save_model():
    '''Save JSONS as services or transactions'''
    form = request.form
    response_response = '{"msg": "form-data not find or malformed"}'
    status = 400

    if 'data_type' in form and 'data' in form:
        data_type = request.form['data_type']
        data = json.loads(request.form['data'])

        model = None
        if data_type == 'service':
            model = service_controller.save_service(CONNECTION, DATABASE, data)
        elif data_type == 'transaction':
            model = transaction_controller.save_transaction(
                CONNECTION,
                DATABASE,
                data
            )

        if model is not None:
            model_response_id = str(model._id)
            response_response = json.dumps({'model_id': model_response_id})
            status = 200

    response = APP.response_class(
        response=response_response,
        status=status,
        mimetype='application/json'
    )

    return response

if __name__ == '__main__':
    APP.run(debug=True)
